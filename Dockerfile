FROM gradle:6.4-jdk8 as build
WORKDIR /app
COPY build.gradle .
RUN gradle build
COPY src src
RUN gradle bootJar
RUN mkdir -p build/dependency && (cd build/dependency; jar -xf ../libs/*.jar)


FROM openjdk:8-jre-alpine as production
RUN addgroup -S rafikic && adduser -S rafikic -G rafikic
USER rafikic:rafikic
ARG DEPENDENCY=app/build/dependency
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app
ENTRYPOINT ["java","-cp","app:/app/lib/*","CardManagerApplication"]