package mars.card.manager.controller;

import mars.card.manager.service.CardService;
import mars.card.manager.entity.Card;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cards")
public class CardController {

    private final CardService service;

    @Autowired
    public CardController(CardService service) {
        this.service = service;
    }

    @GetMapping
    public List<Card> getAll(){
        return service.getAll();
    }

    @PostMapping
    public Card addCard(@RequestBody Card card){
        return service.addCard(card);
    }
}
