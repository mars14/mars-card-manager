package mars.card.manager.entity;

import mars.card.manager.entity.enums.FieldType;
import mars.card.manager.entity.enums.Symbol;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
public class AreaRequirements {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Enumerated(EnumType.STRING)//available field type
    private FieldType fieldType;
    @Enumerated(EnumType.STRING)//required neighbor area
    private Symbol requiredNeighbor;
    private int neighborQuantity;
    @Enumerated(EnumType.STRING)//forbidden neighbor area
    private Symbol forbiddenNeighbor;
    @ElementCollection(targetClass = Symbol.class)
    @Enumerated(EnumType.STRING)
    private List<Symbol> requiredBonuses;

}
