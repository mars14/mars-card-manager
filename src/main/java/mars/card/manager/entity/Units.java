package mars.card.manager.entity;

import mars.card.manager.entity.enums.Symbol;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
public class Units {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Enumerated(EnumType.STRING)
    private Symbol symbol;
    private int quantity;
    private boolean irremovable;

    public Units(Symbol symbol, int quantity) {
        this.symbol = symbol;
        this.quantity = quantity;
    }
}
