package mars.card.manager.entity;

import mars.card.manager.entity.enums.CardType;
import mars.card.manager.entity.enums.Edition;
import mars.card.manager.entity.enums.Symbol;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor//TODO: remove redundant annotation
@Data
public class Card {

    @Id
    private String title;
    @Enumerated(EnumType.STRING)
    private CardType cardType;
    @Enumerated(EnumType.STRING)
    private Edition edition;
    private int cost;
    @ElementCollection(targetClass = Symbol.class)
    @Enumerated(EnumType.STRING)
    private List<Symbol> categories;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Requirement> requirements;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Effect> effects;
    @OneToOne(cascade = CascadeType.ALL)
    private Points points;
//    private boolean actionDone;
//    @OneToOne
//    private Units units;

}
