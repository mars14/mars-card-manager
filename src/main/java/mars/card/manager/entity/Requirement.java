package mars.card.manager.entity;

import mars.card.manager.entity.enums.LimitType;
import mars.card.manager.entity.enums.Symbol;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
public class Requirement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Enumerated(EnumType.STRING)
    private Symbol symbol;
    private int value;
    @Enumerated(EnumType.STRING)
    private LimitType limitType;

}
