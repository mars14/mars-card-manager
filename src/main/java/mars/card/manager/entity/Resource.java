package mars.card.manager.entity;

import mars.card.manager.entity.enums.ResourceMode;
import mars.card.manager.entity.enums.ResourceType;
import mars.card.manager.entity.enums.Symbol;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
public class Resource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Enumerated(EnumType.STRING)
    private Symbol symbol;
    private int value;
    @Enumerated(EnumType.STRING)
    private ResourceType resourceType;
    @ElementCollection(targetClass = ResourceMode.class)
    @Enumerated(EnumType.STRING)
    private List<ResourceMode> resourceModes;
    @OneToOne(cascade = CascadeType.ALL)
    private AreaRequirements areaRequirements;
    @Enumerated(EnumType.STRING)
    private Symbol alternateSymbol;

}
