package mars.card.manager.entity.enums;

public enum EffectType {
    IMMEDIATE, ACTION, PERMANENT
}
