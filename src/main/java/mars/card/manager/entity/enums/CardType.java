package mars.card.manager.entity.enums;

public enum CardType {
    CORPORATION, BLUE, GREEN, RED
}
