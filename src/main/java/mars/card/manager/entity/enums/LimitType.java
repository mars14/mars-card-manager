package mars.card.manager.entity.enums;

public enum LimitType {
    MIN, MAX
}
