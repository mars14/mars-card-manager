package mars.card.manager.entity.enums;

public enum FieldType {
    GROUND, OCEAN, VOLCANO, NOCTIS_CITY, GANYMEDE, PHOBOS
}
