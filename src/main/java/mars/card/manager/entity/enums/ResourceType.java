package mars.card.manager.entity.enums;

public enum ResourceType {
    UNIT, PRODUCTION, CATEGORY, BONUS, EXCHANGE_FACTOR, EXCHANGE_VALUE
}
