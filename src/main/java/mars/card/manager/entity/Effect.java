package mars.card.manager.entity;

import mars.card.manager.entity.enums.EventType;
import mars.card.manager.entity.enums.EffectType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
public class Effect {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Enumerated(EnumType.STRING)
    private EffectType effectType;
    @ElementCollection(targetClass = EventType.class)
    @Enumerated(EnumType.STRING)
    private List<EventType> eventTypes;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Resource> benefitResources;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Resource> requiredResources;
    private boolean optional;

}
