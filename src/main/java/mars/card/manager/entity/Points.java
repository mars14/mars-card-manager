package mars.card.manager.entity;

import mars.card.manager.entity.enums.Symbol;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
public class Points {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int multiplier;
    private int divider;
    @Enumerated(EnumType.STRING)
    private Symbol symbol;

}
